<?php

/**
 * @file Theme overrides for CCK-6.x-2.x
 */

/**
 * Theme override for theme('content_multiple_value') from content.module.
 *
 * Fields with multiple values are rendered as tables and the description needs
 * to be rendered as part of this, not after, as the default is.
 *
 * @see content.module
 */
function theme_element_shuffle_cck2_content_multiple_values($element) {
  $field_name = $element['#field_name'];
  $field = content_fields($field_name);
  $output = '';

  if ($field['multiple'] >= 1) {
    $table_id = $element['#field_name'] .'_values';
    $order_class = $element['#field_name'] .'-delta-order';
    $required = !empty($element['#required']) ? '<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';
    $item_classes = 'content-multiple-table';
    $desc_classes = 'description';
    $toggle_button = '';
    $toggle_desc = ($field['widget']['toggle_description'] and $element['#description']);
    if ($toggle_desc) {
      $toggle_group = element_shuffle_description_factory();
      $item_classes .= ' toggle-description '      . $toggle_group;
      $desc_classes .= ' toggle-description-desc ' . $toggle_group;
      $toggle_button = '<span class="toggle-description-button-container '. $toggle_group .'"></span>';
    }
    $desc = $element['#description'] ? '<div class="'. $desc_classes .'">'. $element['#description'] .'</div>' : '';
    $desc_above = ($element['#description'] and !empty($field['widget']['description_above']));
    $header = array(
      array(
        'data' => $toggle_button . t('!title: !required', array('!title' => $element['#title'], '!required' => $required)),
        'colspan' => 2,
      ),
      t('Order'),
    );
    $rows = $desc_above
      ? array(array('class' => 'toggle-description-row',
                    'data'  => array(array('data' => $desc, 'colspan' => 4))))
      : array();

    // Sort items according to '_weight' (needed when the form comes
    // back after
    // preview or failed validation)
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key !== $element['#field_name'] .'_add_more') {
        $items[] = &$element[$key];
      }
    }
    usort($items, '_content_sort_items_value_helper');

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      $item['_weight']['#attributes']['class'] = $order_class;
      $delta_element = drupal_render($item['_weight']);
      $cells = array(
        array('data' => '', 'class' => 'content-multiple-drag'),
        drupal_render($item),
        array('data' => $delta_element, 'class' => 'delta-order'),
      );
      $rows[] = array(
        'data' => $cells,
        'class' => 'draggable',
      );
    }

    $output .= theme('table', $header, $rows, array('id' => $table_id, 'class' => $item_classes));
    $output .= $desc_above ? '' : $desc;
    $output .= drupal_render($element[$element['#field_name'] .'_add_more']);

    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }

  return $output;
}
