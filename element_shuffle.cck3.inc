<?php

/**
 * @file Theme overrides for CCK-6.x-3.x
 */

/**
 * Theme override for theme('content_multiple_value') from
 * content.module.
 *
 * Fields with multiple values are rendered as tables and the description needs
 * to be rendered as part of this, if set to be before the elements
 *
 * @see content.module
 */
function theme_element_shuffle_cck3_content_multiple_values($element) {
  $field_name = $element['#field_name'];
  $field = content_fields($field_name);
  $output = '';

  if ($field['multiple'] >= 1) {
    // Try harder to find the widget for this field.
    if (! empty($element[0])) {
      $content_type = content_types($element[0]['#type_name']);
      $widget = $content_type['fields'][$field_name]['widget'];
    } else {
      $widget = $field['widget'];
    }
    $table_id = $element['#field_name'] .'_values';
    $order_class = $element['#field_name'] .'-delta-order';
    $required = !empty($element['#required']) ? '<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';
    $item_classes = 'content-multiple-table';
    $desc_classes = 'description';
    $toggle_button = '';
    $toggle_desc = ($widget['toggle_description'] and $element['#description']);
    if ($toggle_desc) {
      $toggle_group = element_shuffle_description_factory();
      $item_classes .= ' toggle-description '      . $toggle_group;
      $desc_classes .= ' toggle-description-desc ' . $toggle_group;
      $toggle_button = '<span class="toggle-description-button-container '. $toggle_group .'"></span>';
    }
    $desc = $element['#description'] ? '<div class="'. $desc_classes .'">'. $element['#description'] .'</div>' : '';
    $desc_above = ($element['#description'] and !empty($widget['description_above']));
    $header = array(
      array(
        'data' => $toggle_button . t('!title: !required', array('!title' => $element['#title'], '!required' => $required)),
        'colspan' => 2,
      ),
      array('data' => t('Order'), 'class' => 'content-multiple-weight-header'),
    );
    if ($field['multiple'] == 1) {
      $header[] = array('data' => '<span>'. t('Remove') .'</span>', 'class' => 'content-multiple-remove-header');
    }
    $rows = $desc_above
      ? array(array('class' => 'toggle-description-row',
                    'data'  => array(array('data' => $desc, 'colspan' => 3))))
      : array();

    // Sort items according to '_weight' (needed when the form comes back after
    // preview or failed validation)
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key !== $element['#field_name'] .'_add_more') {
        $items[$element[$key]['#delta']] = &$element[$key];
      }
    }
    uasort($items, '_content_sort_items_value_helper');

    // Add the items as table rows.
    foreach ($items as $delta => $item) {
      $item['_weight']['#attributes']['class'] = $order_class;
      $delta_element = drupal_render($item['_weight']);
      if ($field['multiple'] == 1) {
        $remove_element = drupal_render($item['_remove']);
      }
      $cells = array(
        array('data' => '', 'class' => 'content-multiple-drag'),
        drupal_render($item),
        array('data' => $delta_element, 'class' => 'delta-order'),
      );
      $row_class = 'draggable';
      if ($field['multiple'] == 1) {
        if (!empty($item['_remove']['#default_value'])) {
          $row_class .= ' content-multiple-removed-row';
        }
        $cells[] = array('data' => $remove_element, 'class' => 'content-multiple-remove-cell');
      }
      $rows[] = array('data' => $cells, 'class' => $row_class);
    }

    $output .= theme('table', $header, $rows, array('id' => $table_id, 'class' => $item_classes));
    $output .= $desc_above ? '' : $desc;
    $output .= drupal_render($element[$element['#field_name'] .'_add_more']);

    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
    drupal_add_js(drupal_get_path('module', 'content') .'/js/content.node_form.js');
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }

  return $output;
}
