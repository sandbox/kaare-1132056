
Drupal.behaviors.elementShuffle = function(context) {
    if (Drupal.settings.elementShuffle.cckVersion == 3) {
	$('table.content-multiple-table:not(.content-multigroup-edit-table-multiple-columns)').find('thead').not('.element-shuffle-colspan-fix-processed').addClass('element-shuffle-colspan-fix-processed').each(function() {
	    $(this).find('th:first').attr('colspan', 3);
	});
    }
    $('.toggle-description').toggleDescription();
};

/**
 * Jquery plugin that makes drupal form element descriptions collapsible
 */
jQuery.fn.toggleDescription = function() {
    this.filter(':not(.toggle-description-processed)').addClass('toggle-description-processed').each(function() {
	var $this = $(this);
	var toggle_group = /(toggle-description-\d+)/.exec($this.attr('class'))[0];
	var $desc = $this.find('.toggle-description-desc.' + toggle_group);
	if (! $desc.length) {
	    return;
	}
	var $help = Drupal.theme('esToggleButton', $desc);
	$this.find('.toggle-description-button-container.' + toggle_group).append($help);
    });
};

Drupal.theme.prototype.esToggleButton = function($target) {
    return $('<a href="#" class="toggle-description-trigger description-collapsed" />')
	.html(Drupal.settings.elementShuffle.buttonShow)
	.click(function() {
	    if ($(this).hasClass('description-collapsed')) {
		$(this).removeClass('description-collapsed').addClass('description-expanded').html(Drupal.settings.elementShuffle.buttonHide);
		$target.slideDown('fast');
	    }
	    else {
		$(this).removeClass('description-expanded').addClass('description-collapsed').html(Drupal.settings.elementShuffle.buttonShow);
		$target.slideUp('fast');
	    }
	    return false;
	});
};
