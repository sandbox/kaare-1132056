
Manipulates the order of form description and input elements, and optionally
make the description collapsible.  This is useful to un-clutter node forms with
a lot of long descriptions.

INSTALL AND USAGE

Enable the module your favourite way:

       $ drush -y en element_shuffle

Configure fields and fieldgroups in content type forms.  Fields will have the
options available in the "Element shuffle" fieldgroup, whereas node fieldgroups
only have the option of making its description toggable.


DEVELOPERS

This module introduces two new FAPI attributes:

 o  #description_above — Move the description above the input element
 o  #toggle_description — Add a JS toggle button making the element
    collapsible/toggable.

For CCK fields, these can be configured on a per file basis, otherwise it's done
the usual FAPI way:

    $form['example'] = array(
      '#type' => 'textfield',
      '#title' => t('Example element'),
      '#description' => t("Long text describing the meaning of example element"),
      '#description_above' => TRUE,
      '#toggle_description' => TRUE,
    );
