<?php

/**
 * @file
 * Menu callbacks for ES
 */

/**
 * ES admin settings form callback
 */
function element_shuffle_settings() {
  $form['labels'] = array(
    '#type' => 'fieldset',
    '#title' => t('Toggle description button labels'),
    '#description' => "The button used to toggle descriptions can have different labels for collapsed and expanded state.",
  );
  $form['labels']['element_shuffle_button_show'] = array(
    '#type' => 'textfield',
    '#title' => 'Show description button label',
    '#default_value' => variable_get('element_shuffle_button_show', "Show description"),
  );

  $form['labels']['element_shuffle_button_hide'] = array(
    '#type' => 'textfield',
    '#title' => 'Hide description button label',
    '#default_value' => variable_get('element_shuffle_button_hide', "Hide description"),
  );

  $cck_ver = element_shuffle_cck_version();
  if ($cck_ver) {
    variable_set('element_shuffle_cck_version', $cck_ver);
  }
  else {
    $form['element_shuffle_cck_version'] = array(
      '#type' => 'select',
      '#title' => t('CCK Version'),
      '#description' => t("Whenever auto-detecting CCK fails, specify the major version here.  It's required for proper theming of multivalue CCK fields."),
      '#description_above' => TRUE,
      '#toggle_description' => TRUE,
      '#default_value' => variable_get('element_shuffle_cck_version', 2),
      '#options' => array(2 => '2.x', 3 => '3.x'),
    );
  }
  // Run system_settings_form_submit() before our submit handler, to make sure
  // we can access the newly stored variable 'element_shuffle_cck_version'
  $form = system_settings_form($form);
  return $form;
}
